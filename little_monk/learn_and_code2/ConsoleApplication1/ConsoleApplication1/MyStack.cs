﻿using System;
using System.Collections;

namespace learn_and_code
{
    class MyStack
    {
        const int indexOfEmptyArray = -1;

        ArrayList list;
        int index;
        int temp;
        public MyStack()
        {
            index = indexOfEmptyArray;
            list = new ArrayList();
        }
        public ArrayList GetList()
        {
            return list;
        }

        public void push(int element)
        {
            list.Add(element);
            index++;
        }

        public int pop()
        {
            if (index != indexOfEmptyArray)
            {
                temp = Convert.ToInt32(list[index]);
                list.RemoveAt(index);
                index--;
                return temp;
            }
            else
            {
                throw new Exception("Stack is empty");
            }
        }

        public void clear()
        {
            list.Clear();
            index = indexOfEmptyArray;
        }
    }
}