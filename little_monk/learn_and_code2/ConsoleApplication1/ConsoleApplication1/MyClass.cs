﻿using System;
using System.Collections;

namespace learn_and_code
{
    class MyClass
    {
        static void Main(string[] args)
        {
            MyStack st = new MyStack();
            ArrayList list = st.GetList();
            int N;
            int T;
            int ID;
            char myevent;
            int id1;
            int id2;
            int[] playersHavingBall;

            T = readT();
            playersHavingBall = new int[T];
            for (int i = 0; i < T; i++)
            {
                string[] Array = Console.ReadLine().Split(' ');
                N = checkForN(int.Parse(Array[0]));
                if (N >= 1 && N <= 100000)
                {
                    ID = checkForId(int.Parse(Array[1]));
                    st.push(ID);
                    for (int j = 0; j < N; j++)
                    {
                        Array = Console.ReadLine().Split(' ');
                        myevent = char.Parse(Array[0]);
                        if (myevent == 'P')
                        {
                            //ID = int.Parse(Array[1]);
                            st.push(int.Parse(Array[1]));
                        }
                        else if (myevent == 'B')
                        {
                            id1 = st.pop();
                            if (list.Count != 0)
                            {
                                id2 = st.pop();
                                st.push(id1);
                                st.push(id2);
                            }
                        }
                    }
                }
                playersHavingBall[i] = st.pop();
            }
            for (int i = 0; i < T; i++)
            {
                Console.WriteLine("Player " + playersHavingBall[i]);
            }
            Console.ReadLine();
        }
        private static int readT()
        {
            int T = Convert.ToInt32(Console.ReadLine());
            if (T >= 1 && T <= 100)
            {
                return T;
            }
            else
            {
                throw new Exception("T is out of range");
            }
        }
        private static int checkForN(int N)
        {
            if (N >= 1 && N <= 100000)
            {
                return N;
            }
            else
            {
                throw new Exception("N is out of range");
            }
        }
        private static int checkForId(int Id)
        {
            if (Id >= 1 && Id <= 1000000)
            {
                return Id;
            }
            else
            {
                throw new Exception("Id is out of range");
            }
        }
    }
}