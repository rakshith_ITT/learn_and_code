﻿using System;
using System.Collections;
namespace learn_and_code2
{
    class MyClass
    {
        static void Main(string[] args)
        {
            stack st = new stack();
            ArrayList list = st.GetList();
            int N;
            int T;
            int ID;
            char myevent;
            int id1;
            int id2;
            int[] playersHavingBall;

            T = Convert.ToInt32(Console.ReadLine());
            playersHavingBall = new int[T];
            if (T >= 1 && T <= 100)
            {
                for (int i = 0; i < T; i++)
                {
                    string[] Array = Console.ReadLine().Split(' ');
                    N = int.Parse(Array[0]);
                    if (N >= 1 && N <= 100000)
                    {

                        ID = int.Parse(Array[1]);
                        if (!(ID >= 1 && ID <= 1000000))
                        {
                            return; // Console.WriteLine("ID is out of range");
                        }
                        st.push(ID);
                        for (int j = 0; j < N; j++)
                        {
                            Array = Console.ReadLine().Split(' ');
                            myevent = char.Parse(Array[0]);
                            if (myevent == 'P')
                            {
                                //ID = int.Parse(Array[1]);
                                st.push(int.Parse(Array[1]));
                            }
                            else if (myevent == 'B')
                            {
                                id1 = st.pop();
                                if (list.Count != 0)
                                {
                                    id2 = st.pop();
                                    st.push(id1);
                                    st.push(id2);
                                }
                            }
                        }
                    }
                    else return; // Console.WriteLine("N is out of Range");
                    playersHavingBall[i] = st.pop();
                }
            }
            else
                return; //Console.WriteLine("T is out of range");
            for(int i = 0; i < T; i++)
            {
                Console.WriteLine("Player " + playersHavingBall[i]);
            }
            Console.ReadLine();
        }
    }
}