﻿using System;
using System.Collections;
namespace learn_and_code1
{
    class MyStack
    {
        const int BottomOfTheStack = -1;
        const int EmptyStackValue = 0;
        int index;
        ArrayList list;
        private int temp;
        public MyStack()
        {
            index = BottomOfTheStack;
            list = new ArrayList();
        }
        public ArrayList GetList()
        {
            return list;
        }
        public void Push(int element)
        {
            list.Add(element);
            index++;
        }
        public int Pop()
        {
            if (index != BottomOfTheStack)
            {
                temp = Convert.ToInt32(list[index]);
                list.RemoveAt(index);
                index--;
                return temp;
            }
            else
                return EmptyStackValue;
        }
        public int Peek()
        {
            if (index == BottomOfTheStack)
                return EmptyStackValue;
            else
            {
                return Convert.ToInt32(list[index]);
            }
        }
        public void Clear()
        {
            list.Clear();
            index = BottomOfTheStack;
        }
    }
    class MyClass
    {
        static void Main(string[] args)
        {
            MyStack myStack = new MyStack();
            ArrayList list = myStack.GetList();
            int size;
            int longestSubArray = 0;
            int subArray = 0;
            int temp;
            size = Convert.ToInt32(Console.ReadLine());
            string[] InputArray = Console.ReadLine().Split(' ');
            int[] integerArray = new int[InputArray.Length];
            for (int i = 0; i < integerArray.Length; i++)
            {
                temp = int.Parse(InputArray[i]);
                if (temp > 0)
                {
                    myStack.Push(temp);
                }
                else if (temp < 0)
                {
                    if (temp == myStack.Pop() * (-1))
                    {
                        if (list.Count == 0)
                        {
                            longestSubArray += 2;
                        }
                        else
                        {
                            subArray += 2;
                            if (longestSubArray < subArray)
                            {
                                longestSubArray = subArray;
                            }
                        }
                    }
                    else
                    {
                        myStack.Clear();
                    }
                }
            }
            Console.WriteLine(longestSubArray);
            Console.ReadLine();
        }
    }
}